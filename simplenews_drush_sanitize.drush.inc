<?php
/**
 * @file simplenews_drush_sanitize.drush.inc
 *
 *
 */

/**
* Implementation of hook_drush_sql_sync_sanitize().
*/
function simplenews_drush_sanitize_drush_sql_sync_sanitize($source) {
  $query  = "UPDATE simplenews_subscriber SET mail = CONCAT(snid, '-snid-@example.com');";
  $query .= "UPDATE simplenews_mail_spool SET mail = CONCAT(snid, '-snid-@example.com');";
  drush_sql_register_post_sync_op('simplenews_drush_sanitize', dt('Sanitize Simplenews email addresses'), $query);
}
